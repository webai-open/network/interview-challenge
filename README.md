# WebAI Network Team Open Interview Challenge

This is an alternative interview process for our team.

Our goal with this challenge is to make our hiring process accessible to people who have the knowledge and passion to build in this space, but who either:

* would rather just cut to the chase and show us they can build this stuff, or
* don't have on-paper resume experience

You can still go through the [front door](https://boards.greenhouse.io/webai) of our standard hiring process if this open challenge isn't your thing.

We currently have the following seats open on our team:

* Staff-level Peer-to-Peer Engineer

# Goal

We want to see that you're capable of doing the stuff you'll be doing when you join our team.

Our team is building a peer-to-peer network.

It's important that you be able to:

* read and write code in a peer-to-peer codebase,
* understand the peer-to-peer landscape,
* read and internalize research papers.

We want to see that you're passionate about this area of research and are capable of turning that passion into product.

# Challenge

Design and implement a p2p protocol.

It can be anything, from a social network to a file sharing app.

It can be as elaborate or as simple as you want.

You can build a solid core and talk about how you'd evolve it, or you can build a full stack.

You can create a full experience or design a collection of building blocks that would enable a peer-to-peer experience.

You can pull in some libraries that do the heavy lifting for you and focus on demonstrating you understand what those libraries are doing and how to vet projects in the existing peer-to-peer landscape.

You can write a paper proposing a novel protocol, along with a proof-of-concept.

Make sure you clearly define the goals of your submission, articulate your decisions, and tell us why you have faith in your implementation or proposal.

# Languages

You can use whatever language you want, but it's important that you write clear and readable code in whatever language you choose.

The less familiar our team is with your language choice, the less you can expect us to understand idiomatic patterns, so place comments explaining the decisions you make.

Our team is most familiar with Rust, Typescript, JavaScript, Go, and Python.

# Inspiration

We'd recommend taking a look at one or more of these papers before diving into an implementation:

* [Secure Scuttlebutt](https://dl.acm.org/doi/pdf/10.1145/3357150.3357396)
* [IPFS](https://gipplab.org/wp-content/papercite-data/pdf/trautwein2022a.pdf)
* [Kademlia](https://www.scs.stanford.edu/~dm/home/papers/kpos.pdf)
* [s/Kademlia](https://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=1343BD690A1DB52C2D59F441E8D5C18E?doi=10.1.1.68.4986&rep=rep1&type=pdf)
* [Storj v2](https://www.storj.io/storjv2.pdf)
* [Capability Myths Demolished](https://srl.cs.jhu.edu/pubs/SRL2003-02.pdf)
* [UCAN Spec](https://github.com/ucan-wg/spec)
* [Merkle-CRDTs](https://hector.link/presentations/merkle-crdts/merkle-crdts.pdf)
* [Filecoin](https://filecoin.io/filecoin.pdf)
* [BitTorrent](https://www.bittorrent.org/bittorrentecon.pdf)

You might also find [libp2p](https://docs.libp2p.io/) useful.

If you know of better papers or projects, feel free to ignore this list and use your own.

# Limits

A submission is not a guarantee of being hired, do not invest any more time in this than you'd be willing to invest if you receive a "no."

If you're already a master of your craft, putting together something quick with a writeup of how you'd evolve it is sufficient to get on our radar.

If you self-identify as a growth hire, but are passionate about ramping up in this space, spend some time growing on this project. Prove you can learn quickly and build. A decent submission with a description of what you've learned and internalized while building is sufficient to get on our radar.

# Questions

The point of contact for this process is William Blankenship, he can be reached at: [william.blankenship@webai.com](mailto:william.blankenship@webai.com)

# Submitting

To submit your solution package it up, along with any documentation and commentary, into a tar.xz and upload it to IPFS.

Email us the CID at [william.blankenship@webai.com](mailto:william.blankenship@webai.com)

Our team works Monday through Thursday.

# Project

We are a small team hyper-focused on bringing peer-to-peer networks to bear on AI. While we believe AI demands a peer-to-peer architecture, we don't think of our peer-to-peer architecture as an AI-specific solution.

Our team is creating a human-centric networking stack that enables digital property rights. Where humans can come to the network and retain ownership of their personal data, with a contract layer enabling the sale of goods and services.

The current high-level building blocks we are building out:

* **Peer-to-Peer Overlay Network**: s/Kademlia based network allowing you to open a bidirectional socket by dialing a public key; support for NAT traversal and TCP/UDP/WebSocket.

* **Content Addressable Asset Discovery**: Given the hash of an asset, all peers that have an instance of it can be discovered on the overlay network.

* **Multi-Writer Append-Only Gossip Database**: A CRDT-blocktree that can be gossiped over the overlay network, with support for multiple private keys on the network adding blocks to the data structure without coordination.

* **Identity**: The above database populated with a user’s device’s public keys, crypto wallet public keys, and other socially communicated data. The root of this tree is advertised on the network as a content addressable hash. If you can find the database, you can find the user’s content and devices, and you can enter into crypto-transactions with them. This makes users, and their content, addressable and discoverable on the network.

* **Capabilities**: A set of scopes and permissions allowing identities to delegate to other identities on the network. For example, Alice may issue a capability to Bob allowing him to add a limited number of entries to one of her gossip databases. Other users can verify these blocks against the capability grant. This allows for delegating responsibilities to 3rd party vendors and modeling organizations for enterprise use cases.

* **Utility Coins**: By adding wallets to user identity trees, we can use multiple fit-for-purpose utility coins for implementing contracts between users and incentivizing behaviors on the network. This data structure unifies identities across WIN, Ethereum, and Filecoin for our contracts. With capabilities, we can limit the scope of contracts a delegated identity can enter into on the network.

With this small collection of protocols and data structures, we believe we can create a human-addressable web. Where humans, and their content, are directly addressable on an overlay network.

With contracts and capabilities, we believe we can invert the relationship between SaaS vendors and user data, allowing users to delegate access to their identity within a capability scope to a 3rd party SaaS vendor.

# Team

We currently have a team of 4 working fully remote. The team tries to keep overlap with both the EU and US for working hours.

We work 4-day weeks and give the space to learn the field and do things right.

Salary is $150k-$225k base.

We are moving fast, but not by throwing hours at the clock. We are staying hyper-focused on foundational, critical-path, work that keeps us moving toward our goal without needing to burn the midnight oil.
